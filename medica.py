import mariadb


# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user="bkoj",
        password="bkoj",
        host="127.0.0.1",
        port=3306,
        database="medicaments"

    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor()



def affichage(reponse):
    for line in reponse:
        print (" | ".join(line))


def get_by_subtance(word,cur):
    sql="SELECT DISTINCT  CIS_bdpm.denomination FROM CIS_bdpm INNER JOIN CIS_compo_bdpm ON CIS_bdpm.code_cis= CIS_compo_bdpm.code_cis WHERE CIS_compo_bdpm.denomination LIKE ?"
    word="%"+word+"%"
    cur.execute(sql,(word,))
    data=cur.fetchall()
    affichage(data)

def get_by_name(word,cur):
    sql="SELECT denomination,voies FROM CIS_bdpm WHERE denomination LIKE ?"
    word="%"+word+"%"
    cur.execute(sql,(word,))
    data=cur.fetchall()
    affichage(data)

def get_by_patho(word,cur):
    sql="SELECT DISTINCT denomination,voies FROM CIS_bdpm INNER JOIN CIS_CIP_bdpm ON CIS_bdpm.code_cis=CIS_CIP_bdpm.code_cis INNER JOIN CIS_has_smr ON CIS_bdpm.code_cis = CIS_has_smr.code_cis  WHERE CIS_has_smr.libelle_smr LIKE ? OR CIS_CIP_bdpm.presentation LIKE ?"
    word="%"+word+"%"
    cur.execute(sql,(word,word))
    data=cur.fetchall()
    affichage(data)

funcs={
    "1":get_by_name,
    "2":get_by_subtance,
    "3":get_by_patho
}


quitter=False

while quitter == False:
    print("\n")
    print("1 - par nom: ")
    print("2 - par substance: ")
    print("3 - par pathologie: ")
    choix=input("Quelle recherche? ( mode/valeur ) : ")
    if choix == "exit":
        quitter=True
    else:
        try:
            splitted=choix.split("/")
            funcs[splitted[0]](splitted[1],cur)
        except:
            print("Choix impossible")

print("")
print("A bientot! ")

conn.close()
